document.addEventListener("DOMContentLoaded", listTabs());
var url = "/test"

function listTabs() {
   browser.extension.getURL("");
   getCurrentWindowTabs().then((tabs) => {
      for (let i = 0; i < tabs.length; i++) {
         if (tabs[i].active) {
            //console.log(tabs[i])
            //executeScript(tabs[i]);
            break;
         }
      }
   });
}

function getCurrentWindowTabs() {
   return browser.tabs.query({ currentWindow: true });
}

function onExecuted(result) {
   console.log(`We executed in tab: ` + result);
}

function onError(error) {
   console.log(`Error: ${error}`);
}

function executeScript(tab) {
   browser.tabs.create({ url: url }).then(() => {
      browser.tabs.executeScript(
         tab.id, {
         code: "console.log('heh')"
      }).then(onExecuted, onError);
   });
}

// $('#button-addon2').click(function () {
//    //chrome.tabs.create({url: 'http://youtube.com'});
//    //console.log("test")
//    //console.log(browser)
//    var test = browser.tabs
//    console.log(querying)
//    // var info = chrome.tabs.Tab();
//    // console.log(info)
//    // console.log(chrome.tabs.getAllInWindow());
// });

// browser.tabs.create({ url: "https://www.google.com/" }).then(() => {
//    browser.tabs.executeScript({
//       code: `console.log('location:', window.location.href);`
//    });
// });

// chrome.tabs.query(
//    { active: true, windowId: chrome.windows.WINDOW_ID_CURRENT },
//    function (tabs) {
//       const { id: tabId } = tabs[0].id;
//       let code = document.querySelector('h1');
//       console.log(code)
//       // http://infoheap.com/chrome-extension-tutorial-access-dom/
//       chrome.tabs.executeScript(tabId, { code }, function (result) {
//          // result has the return value from `code`
//          console.log("test");
//       });
//    }
// );

chrome.runtime.onMessage.addListener(function (request, sender) {
   if (request.action == "getSource") {
      console.log(request.source);
   }
});

chrome.tabs.executeScript(null, {
   file: "script.js"
}, function () {
   if (chrome.runtime.lastError) {
      console.log('There was an error injecting script : \n' + chrome.runtime.lastError.message)
   }
});